﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OlavMartosP7M03Uf5
{
    public class Pokemon
    {
        // Atributos con sus Getters y Setters
        private int pokedex_number;

        public int Getpokedex_number()
        {
            return pokedex_number;
        }

        public void Setpokedex_number(int value)
        {
            pokedex_number = value;
        }

        private string name;

        public string Getname()
        {
            return name;
        }

        public void Setname(string value)
        {
            name = value;
        }

        private int generation;

        public int Getgeneration()
        {
            return generation;
        }

        public void Setgeneration(int value)
        {
            generation = value;
        }

        private string type1;

        public string Gettype1()
        {
            return type1;
        }

        public void Settype1(string value)
        {
            type1 = value;
        }

        private string type2;

        public string Gettype2()
        {
            return type2;
        }

        public void Settype2(string value)
        {
            type2 = value;
        }

        private int is_legendary;

        public int Getis_legendary()
        {
            return is_legendary;
        }

        public void Setis_legendary(int value)
        {
            is_legendary = value;
        }

        // Constructores
        // Constructor con todos los parametros
        public Pokemon(int pokedex_number, string name, int generation, string type1, string type2, int is_legendary)
        {
            Setpokedex_number(pokedex_number);
            Setname(name);
            Setgeneration(generation);
            Settype1(type1);
            Settype2(type2);
            Setis_legendary(is_legendary);
        }
        // Constructor para los Pokemon que tienen solo un tipo
        public Pokemon(int pokedex_number, string name, int generation, string type1, int is_legendary)
        {
            Setpokedex_number(pokedex_number);
            Setname(name);
            Setgeneration(generation);
            Settype1(type1);
            Setis_legendary(is_legendary);
        }

        // Metodos
        public override string ToString()
        {
            string legendary = "No es legendario"; ;

            // Si Is_Legendary es diferente a 0 el texto cambiara para indicar que se trata de un pokemon legendario
            if (Getis_legendary() != 0) { legendary = "Es legendario"; }
            return $"Pokemon: {Getpokedex_number()}. {Getname()}. Generacion {Getgeneration()}. Tipo/s {Gettype1()} {Gettype2()}. {legendary}";
        }
    }
}
