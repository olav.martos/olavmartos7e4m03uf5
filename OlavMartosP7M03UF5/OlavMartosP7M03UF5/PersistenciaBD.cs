﻿using System;
using System.Collections.Generic;
using System.IO;
using Npgsql;

namespace OlavMartosP7M03Uf5
{
    class PersistenciaBD
    {
        // Main program.
        static void Main()
        {
            // Lista de Pokemon
            List<Pokemon> instanciasPokemon = new List<Pokemon>();

            // Creates and checks for the connection.
            NpgsqlConnection con = Connection("testdb");
            con.Open();
            if (!CheckStateConnection(con)) return;

            // Does actions, then closes the connection.
            List<string> stats = new List<string>() { "abilities", "against_bug", "against_dark", "against_dragon", "against_electric", "against_fairy", "against_fight", "against_fire", "against_flying", "against_ghost", "against_grass", "against_ground", "against_ice", "against_normal", "against_poison", "against_psychic", "against_rock", "against_steel", "against_water", "attack", "base_egg_steps", "base_happiness", "base_total", "capture_rate", "classfication", "defense", "experience_growth", "height_m", "hp", "name", "percentage_male", "pokedex_number", "sp_attack", "sp_defense", "speed", "type1", "type2", "weight_kg", "generation", "is_legendary" };
            string tableName = "Pokemon";
            
            // Creamos la tabla pokemon y le añadimos los registros
            CreateTable(con, tableName, stats);
            LoadFile(con, tableName, stats);

            // Hacemos un SELECT de la tabla y mostramos los nombres de los pokemon
            SelectPokemon(con, tableName);

            // Creamos instancias segun los registros, los guardamos en la lista instanciasPokemon y la mostramos
            Instancias(con, tableName, instanciasPokemon);
            SeeList(instanciasPokemon);

            // Actualizamos a Charmander
            UpdatePK(con, tableName, instanciasPokemon, "Charmander");
            
            // Eliminamos a los legendarios de la base de datos y mostramos el resultado
            DeleteLegendaryBD(con, tableName);
            SelectPokemon(con, tableName);

            // Creamos una nueva tabla para los legendarios y añadimos los pokemon legendarios y mostramos dicha tabla
            List<string> statsLegends = new List<string>() { "pokedex_number", "name", "generation", "type1", "type2" };
            LegendaryTable(con, "Pokemon_Legendarios", statsLegends, instanciasPokemon);
            SelectPokemon(con, "Pokemon_Legendarios");

            // Despedida
            Console.WriteLine("Adios");
            
            con.Close();
        }

        // Creamos la tabla de pokemon legendarios y añadimos registros
        private static void LegendaryTable(NpgsqlConnection con, string tableName, List<string> statsLegends, List<Pokemon> instanciasPokemon)
        {
            CreateTable(con, tableName, statsLegends);
            var sql = $"INSERT INTO {tableName}(pokedex_number, name, generation, type1, type2) VALUES(@c1, @c2, @c3, @c4, @c5)";

            // Buscamos los pokemon que tengan un uno en su atributo de legendario y los añadimos a la nueva Tabla
            foreach (Pokemon pokemon in instanciasPokemon)
            {
                if(pokemon.Getis_legendary() == 1)
                {
                    using var cmd = new NpgsqlCommand(sql, con);
                    // Primer Insert
                    cmd.Parameters.AddWithValue("c1", pokemon.Getpokedex_number());
                    cmd.Parameters.AddWithValue("c2", pokemon.Getname());
                    cmd.Parameters.AddWithValue("c3", pokemon.Getgeneration());
                    cmd.Parameters.AddWithValue("c4", pokemon.Gettype1());
                    cmd.Parameters.AddWithValue("c5", pokemon.Gettype2());
                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        // Eliminamos a los pokemon legendarios de la BD Pokemon
        private static void DeleteLegendaryBD(NpgsqlConnection con, string tableName)
        {
            using var cmd = new NpgsqlCommand();
            cmd.Connection = con;
            cmd.CommandText = $"DELETE FROM {tableName} WHERE is_legendary = @isLegend";
            cmd.Parameters.AddWithValue("isLegend", "1");
            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        // Actualizamos a un pokemon
        private static void UpdatePK(NpgsqlConnection con, string tableName, List<Pokemon> instanciasPokemon, string pokemonChange)
        {
            var sql = "UPDATE pokemon SET type1 = @type1 WHERE name = @name";
            using var cmd = new NpgsqlCommand(sql, con);
            // Primer update
            cmd.Parameters.AddWithValue("type1", "fuego");
            cmd.Parameters.AddWithValue("name", pokemonChange);
            cmd.Prepare();
            cmd.ExecuteNonQuery();

            foreach (Pokemon pk in instanciasPokemon)
            {
                if(pk.Getname() == $"{pokemonChange}                                                                                                                                                                                                                                                ")
                {
                    pk.Settype1("fuego");
                    Console.WriteLine(pk.ToString());
                    break;
                }
            }
        }

        // Mostramos la infromacion de las instancias de la clase Pokemon
        private static void SeeList(List<Pokemon> instanciasPokemon)
        {
            foreach (Pokemon pk in instanciasPokemon)
            {
                Console.WriteLine(pk.ToString());
            }
            Console.WriteLine($"Instancias creadas: {instanciasPokemon.Count}");
        }

        /// <summary>
        /// Creamos las instancias en base a la linea del lector de BD. Y añade esas instancias a una lista de instancias
        /// </summary>
        /// <param name="instanciasPokemon">Lista de instancias</param>
        /// <param name="rdr">Lector de BD</param>
        private static void Instancias(NpgsqlConnection con, string tableName, List<Pokemon> instanciasPokemon)
        {
            string sql = $"SELECT * FROM {tableName}";
            using var cmd = new NpgsqlCommand(sql, con);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            int contador = 0;

            while (rdr.Read())
            {
                // El contador tiene su utilidad debido a que la primera linea es "name" y eso provoca algunos errores, pero el resto de lineas es Pokemon
                if (contador > 0) { CrearInstancias(instanciasPokemon, rdr); }
                contador++;
            }
            
        }

        // Creamos instancias de la clase Pokemon
        private static void CrearInstancias(List<Pokemon> instanciasPokemon, NpgsqlDataReader rdr)
        {
            Pokemon nuevoPk;
            // Si el campo de type2, no lo tiene ese pokemon usara el segundo constructor, sino el primer constructor definido por la clase Pokemon 
            if (rdr.FieldCount == 39)
            {
                nuevoPk = new Pokemon(Convert.ToInt32(rdr["pokedex_number"]), rdr["name"].ToString(), Convert.ToInt32(rdr["generation"]), rdr["type1"].ToString(), Convert.ToInt32(rdr["is_legendary"]));
            }
            else
            {
                nuevoPk = new Pokemon(Convert.ToInt32(rdr["pokedex_number"]), rdr["name"].ToString(), Convert.ToInt32(rdr["generation"]), rdr["type1"].ToString(), rdr["type2"].ToString(), Convert.ToInt32(rdr["is_legendary"]));
            }

            instanciasPokemon.Add(nuevoPk);
        }

        // Shows all pokemon on the pokemon table
        private static void SelectPokemon(NpgsqlConnection con, string tableName)
        {
            string sql = $"SELECT * FROM {tableName}";
            using var cmd = new NpgsqlCommand(sql, con);
            using NpgsqlDataReader rdr = cmd.ExecuteReader();
            // Shows all pokemons via console.
            while (rdr.Read())
            {
                string pokemonName = rdr["name"].ToString();
                Console.WriteLine(pokemonName);
            }
        }

        // Loads the .csv file, cleans it up and adds the values to the SQL table.
        private static void LoadFile(NpgsqlConnection con, string tableName, List<string> stats)
        {
            // File path.
            string filePath = @"..\..\..\pokemon.csv";

            // Check if the CSV file exists.
            if (!File.Exists(filePath))
            {
                // Message stating CSV file could not be located.
                Console.WriteLine("Could not locate the CSV file.");
                return;
            }

            // Assign the CSV file to a reader object.
            StreamReader reader = new StreamReader(filePath);
            string[] currentRow;
            string line;

            // Cleans the .CSV file.
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Replace("['", "").Replace("']", "");
                currentRow = line.Split(';');
                InsertDataPokemon(currentRow, con, tableName, stats);
            }
        }

        // Creates the pokemon table.
        private static void CreateTable(NpgsqlConnection con, string tableName, List<string> stats)
        {
            // Creates the SQL table
            string sql = $"DROP TABLE IF EXISTS {tableName}; CREATE TABLE {tableName} ({string.Join(" CHAR(250), ", stats.ToArray())} CHAR (250))";

            // Sends the create query.
            NpgsqlCommand commmand = new NpgsqlCommand(sql, con);
            commmand.ExecuteNonQuery();
        }

        // Inserts all the values into the pokemon table.
        private static void InsertDataPokemon(string[] pokemonRow, NpgsqlConnection con, string tableName, List<string> stats)
        {
            // Opens a connection and inserts pokemon data.
            string sql = $"INSERT INTO {tableName}({string.Join(",", stats.ToArray())}) VALUES(@{string.Join(",@", stats.ToArray())})";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

            // Adds the values and prepares them.
            for (int i = 0; i < stats.Count; i++) { cmd.Parameters.AddWithValue(stats[i], pokemonRow[i]); }
            cmd.Prepare();

            // Executes the command and closes the connection.
            cmd.ExecuteNonQuery();
        }

        // Checks the connection state, and returns true/false if open.
        private static bool CheckStateConnection(NpgsqlConnection con)
        {
            if (con.State == System.Data.ConnectionState.Open)
            {
                Console.WriteLine("Conectado");
                return true;
            }
            return false;
        }

        // Creates a connection and returns it.
        private static NpgsqlConnection Connection(string dbName)
        {
            string cs = $"Host=localhost;Username=postgres;Password=postgres;Database={dbName}";
            NpgsqlConnection con = new NpgsqlConnection(cs);
            return con;
        }
    }
}