﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UF5CollectionRepartoJustFeed
{
    class Pedido
    {
        public string nombreCliente { get; set; }
        public string direccionEntrega { get; set; }
        public List<string> listaPlatos { get; set; }

        public Pedido(string nombre, string direccion, List<string> platos)
        {
            nombreCliente = nombre;
            direccionEntrega = direccion;
            listaPlatos = platos;
        }

        public override string ToString()
        {
            string pedido = $"{nombreCliente} quien vive en {direccionEntrega} ha pedido ";

            if (listaPlatos.Count == 1)
            {
                pedido += listaPlatos[0];
            }
            else
            {
                foreach (string plato in listaPlatos)
                {
                    if (plato == listaPlatos[listaPlatos.Count - 1])
                    {
                        pedido += plato;
                    }
                    else
                    {
                        pedido += plato + ", ";
                    }
                }
            }
            return pedido;
        }
    }
}
