﻿using System;
using System.Collections.Generic;

namespace UF5CollectionRepartoJustFeed
{
    class Program
    {
        static void Main(string[] args)
        {
            ColaReparto cola = new ColaReparto();
            string decision = "no";
            do
            {
                /* Iniciamos el bucle preguntando el nuevo pedido, luego mostramos la siguiente
                  y preguntamos si se ha atendido el primer pedido*/
                Console.Clear();
                NuevoPedido(cola);
                cola.MostrarProximoPedido();
                PedidoEntregado(cola);

                // Preguntamos al usuario si quiere salir del programa o no
                Console.Write("Desea salir del programa? (Si/No) ___ ");
                decision = Console.ReadLine();
            } while (decision.ToLower() != "si");

            endExercise();

        }

        /// <summary>
        /// Codigo que se ejecuta para saber que el programa ha terminado
        /// </summary>
        static void endExercise()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa una tecla per terminar");
            Console.ForegroundColor = ConsoleColor.White;
            return;
        }


        /// <summary>
        /// Creamos un nuevo pedido, preguntando el nombre del cliente, la direccion de entrega y la lista de platos pedidos;
        /// y lo añadimos a la cola
        /// </summary>
        /// <param name="cola">Cola de pedidos pendientes</param>
        static void NuevoPedido(ColaReparto cola)
        {
            Console.WriteLine("Nueva pedido");
            Console.Write("Nombre del cliente: ");
            string cliente = Console.ReadLine();
            Console.Write("Direccion de entrega: ");
            string direccion = Console.ReadLine();
            Console.WriteLine("Comenzamos con la lista de platos a pedir");
            List<string> platos = new List<string>();

            string listaCompleta = "no";

            while (listaCompleta.ToLower() != "si")
            {
                Console.Write("Plato a pedir: ");
                platos.Add(Console.ReadLine());

                Console.Write("\nHa completado su lista? (Si/No) ___ ");
                listaCompleta = Console.ReadLine();
            }
            cola.AgregarPedido(cliente, direccion, platos);
        }

        /// <summary>
        /// Preguntamos si se ha entregado el primer y si ha sido entregado se eliminara de la cola.
        /// </summary>
        /// <param name="cola">Cola de solicitudes pendientes</param>
        static void PedidoEntregado(ColaReparto cola)
        {
            Console.WriteLine("Se ha atendido su solicitud?");
            string respuesta = Console.ReadLine();
            bool atendida = false;
            switch (respuesta.ToLower())
            {
                case "si":
                case "yes":
                case "verdadero":
                case "true":
                case "positivo":
                    atendida = true;
                    break;
                case "no":
                case "falso":
                case "false":
                case "negativo":
                    atendida = false;
                    break;
                default:
                    break;
            }

            // Si ha sido entregado, lo eliminamos de la cola
            if (atendida)
            {
                cola.EliminarProximoPedido();
            }

        }

    }
}
