﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UF5CollectionRepartoJustFeed
{
    class ColaReparto : Queue<Pedido>
    {
        Queue<Pedido> cola { get; set; }

        public ColaReparto()
        {
            cola = new Queue<Pedido>();
        }

        public void AgregarPedido(string nombreCliente, string direccionEntrega, List<string> listaPlatos)
        {
            Pedido nuevoPedido = new Pedido(nombreCliente, direccionEntrega, listaPlatos);
            cola.Enqueue(nuevoPedido);
        }

        public void MostrarProximoPedido() { Console.WriteLine($"El siguiente pedido es: {cola.Peek()}"); }

        public void EliminarProximoPedido()
        {
            Pedido eliminado = cola.Dequeue();
            Console.WriteLine($"La solicitud de {eliminado.nombreCliente} ha sido eliminado de la lista");
        }
    }
}
