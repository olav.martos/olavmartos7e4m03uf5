﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ExercicisExcepcions
{
    class ExercicisExcepcions
    {
        static void Main(string[] args)
        {
            var menu = new ExercicisExcepcions();
            menu.Menu();
        }

        public void Menu()
        {
            var option = "";
            string[] exercise = { "Sortir del menu", "Divideix o Cero", "Pasar a Double o 1.0", "Llegir un fitxer", "Aplicació lliure" };
            do
            {
                Console.Clear();
                showExercise(exercise);
                Console.Write("Escull una opcio: ");

                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        Console.Clear();
                        programExecuted(exercise, option);
                        Excepcio1();
                        break;
                    case "2":
                        Console.Clear();
                        programExecuted(exercise, option);
                        Excepcio2();
                        break;
                    case "3":
                        Console.Clear();
                        programExecuted(exercise, option);
                        Excepcio3();
                        break;
                    case "4":
                        Console.Clear();
                        programExecuted(exercise, option);
                        Excepcio4();
                        break;
                    case "0":
                        Console.Clear();
                        endExercise();
                        Environment.Exit(0);
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opció incorrecta!");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
                Console.ReadLine();
            } while (option != "0");

        }

        public void showExercise(string[] ex)
        {
            for (int i = 0; i < ex.Length; i++) Console.WriteLine($"{i}. - {ex[i]}");
        }

        public void endExercise()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa una tecla per terminar");
            Console.ForegroundColor = ConsoleColor.White;
            return;
        }

        public void programExecuted(string[] exercise, string option)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            int index = Convert.ToInt32(option);
            Console.Write("Programa executat: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{exercise[index]}");
            Console.ResetColor();
        }

        /// <summary>
        /// Excepcion numero 1 a dar. Ejecuta una funcion que divide dos numeros
        /// </summary>
        public void Excepcio1()
        {
            Console.WriteLine(DivideixOCero(7, 2));
            Console.WriteLine(DivideixOCero(8, 4));
            Console.WriteLine(DivideixOCero(5, 0));
            endExercise();
        }

        /// <summary>
        /// Funcion que divide dos numeros
        /// </summary>
        /// <param name="dividend">Dividendo</param>
        /// <param name="divisor">Divisor</param>
        /// <returns>Resultado o 0</returns>
        public int DivideixOCero(int dividend, int divisor)
        {
            int resp;
            try { resp = dividend / divisor; }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e);
                resp = 0;
            }

            return resp;

        }

        /// <summary>
        /// Excepcion numero 2 a dar. Ejecuta una funcion que transforma cualquier cosa pasada como parametro a double
        /// </summary>
        public void Excepcio2()
        {
            Console.WriteLine(ADoubleOU(.2));
            Console.WriteLine(ADoubleOU("tres"));
            endExercise();
        }

        /// <summary>
        /// Transforma cualquier cosa pasada como parametro a double. Si no se ha podido hacer la conversion dara 1.0
        /// </summary>
        /// <param name="obj">Objeto pasado como parametro</param>
        /// <returns>Devuelve el double convertido</returns>
        public double ADoubleOU(object obj)
        {
            double resp;
            try
            {
                resp = Convert.ToDouble(obj);
                return resp;
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
                return 1.0;
            }
        }

        /// <summary>
        /// Lee y muestra el contenido de un fichero, pero si este no existe ha de mostrar un mensaje de error y el error dado por el sistema
        /// </summary>
        public void Excepcio3()
        {
            string path = @"E:\cs\pruebas\noExisto.txt";

            try
            {
                StreamReader sr = File.OpenText(path);
                string st = sr.ReadToEnd();
                sr.Close();
                Console.WriteLine(st);
            }
            catch (FileNotFoundException e) { Console.WriteLine($"S'ha produït un error d'entrada/sortida\n{e}"); }
            endExercise();
        }

        /// <summary>
        /// Codigo que comprueba dos excepciones gracias a dos opciones, añadir elementos y borrar elementos en una MutableList
        /// </summary>
        public void Excepcio4()
        {
            List<string> myList = new List<string>();
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("Llista actual: ");
                foreach (string item in myList)
                {
                    Console.WriteLine(item);
                }

                string[] options = { "Afegir un element", "Esborrar un element", "Sortir" };
                Console.WriteLine("Selecciona una opció:");
                showExercise(options);
                Console.Write("> ");

                int option = Convert.ToInt32(Console.ReadLine());

                switch (option)
                {
                    case 0:
                        try
                        {
                            Console.WriteLine("Introdueix un element:");
                            string newItem = Console.ReadLine();
                            myList.Add(newItem);
                        }
                        catch (ArgumentNullException ex)
                        {
                            Console.WriteLine("Error: " + ex.Message);
                            flag = false;
                        }
                        Console.Clear();
                        break;
                    case 1:
                        try
                        {
                            Console.WriteLine("Introdueix l'índex de l'element a esborrar:");
                            int index = int.Parse(Console.ReadLine());
                            myList.RemoveAt(index);
                        }
                        catch (ArgumentOutOfRangeException ex)
                        {
                            Console.WriteLine("Error: " + ex.Message);
                            flag = false;
                        }
                        Console.Clear();
                        break;
                    case 2:
                        Console.WriteLine("Fins aviat!");
                        flag = false;
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Opció no vàlida.");
                        Console.Clear();
                        break;
                }
            }
            endExercise();
        }
    }
}
