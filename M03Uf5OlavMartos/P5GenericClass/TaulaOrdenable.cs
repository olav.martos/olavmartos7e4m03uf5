﻿using System;

namespace P5GenericClass
{
    class TaulaOrdenable<T> where T : IOrdenable
    {
        // Atributos
        private T[] taula;

        // Constructor
        public TaulaOrdenable(T[] taulaEspecifica)
        {
            taula = taulaEspecifica;
        }

        // Metodos

        /// <summary>
        /// Muestra la capacidad de la tabla
        /// </summary>
        /// <returns>Devuelve la longitud de la tabla</returns>
        public int Capacitat()
        {
            return taula.Length;
        }

        /// <summary>
        /// Muestra el numero de elementos, que normalmente seria el mismo que el que ofrece Capacidad()
        /// </summary>
        /// <returns>Devuelve el numero de elementos</returns>
        public int NrElements()
        {
            int cont = 0;
            for(int i = 0; i < taula.Length; i++)
            {
                if (taula[i] != null)
                {
                    cont++;
                }
            }
            return cont;
        }

        /// <summary>
        /// Permite añadir un nuevo objeto a la tabla de objetos
        /// </summary>
        /// <param name="obj">Objeto que queremos añadir</param>
        /// <returns>Devuelve un numero de tres posibilidades. 0, se añadido correctamente, -1 Si el objeto pasado es de tipo null. 
        /// -2 Si no hay mas espacio, si la capacidad y el numero de elementos es igual</returns>
        public int Afegir(T obj)
        {
            if (obj == null) { return -1; }
            if (NrElements() == taula.Length) { return -2; }
            for(int i = 0; i < taula.Length; i++)
            { 
                if (taula[i] == null)
                {
                    taula[i] = obj;
                    return 0;
                }
            }

            return 0;
        }

        /// <summary>
        /// Funcion que busca el ejemplar con una determinada posicion. Hay que recordar que puede devolver nulo o porque es erronea o porque
        /// por el momento esa posicion de la tabla no esta ocupada por ninguna informacion
        /// </summary>
        /// <param name="position">Posicion que queremos examinar</param>
        /// <returns>Nulo o el objeto retornado</returns>
        public T ExemplarAT(int position)
        {
            if(position > taula.Length && position > -1) { return default; }
            return taula[position];
        }

        /// <summary>
        /// Elimina una de las posiciones de la tabla de objetos
        /// </summary>
        /// <param name="position">Posicion que queremos eliminar de la tabla</param>
        /// <returns>Nulo o el objeto elimina</returns>
        public T ExtreureAT(int position)
        {
            if (position > taula.Length && position > -1) { return default; }

            T temp = ExemplarAT(position);
            T[] newArray = new T[taula.Length-1];

            if (position > 0) { Array.Copy(taula, 0, newArray, 0, position); }
            if (position < taula.Length - 1)
            { Array.Copy(taula, position + 1, newArray, position, taula.Length - position - 1); }

            taula = newArray;
            return temp;
        }

        /// <summary>
        /// Funcion que vacia la tabla desde el principio hasta el final. Su capacidad no se ve afectada pero si su Numero de Elementos
        /// </summary>
        public void Buidar() { Array.Clear(taula, 0, taula.Length-1); }

        /// <summary>
        /// Funcion que muestra por pantalla el contenido de la tabla junto a su capacidad y el numero de elementos que contiene dicha tabla.
        /// </summary>
        public void Visualitzar()
        {
            Console.WriteLine("Capacidad de la tabla: " + Capacitat());
            Console.WriteLine("Numero de elementos: " + NrElements());
            int conta = 1;

            // Bucle que recorre la tabla y enumera cada elemento mostrado por pantalla con un numero comenzando por el numero 1 que
            // corresponde al objeto en la posicion 0 de la tabla
            foreach (T cont in taula) { 
                if (cont != null) { Console.WriteLine($"{conta}.- {cont}"); } 
                else { Console.WriteLine($"{conta}.- Objeto no definido"); }
                conta++;
            }
        }

        /// <summary>
        /// Funcion que ordena la tabla usando el metodo Ordenar de la clase Algorismes
        /// </summary>
        public void Ordenar() { Algorismes.Ordenar(taula); }

    }
}
