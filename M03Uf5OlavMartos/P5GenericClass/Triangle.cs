﻿using System.Drawing;

namespace P5GenericClass
{
    public class Triangle : FiguraGeometrica, IOrdenable
    {
        // Atributos
        private double triBase;
        public double GetBase() { return triBase; }
        public void SetBase(double value) { triBase = value; }


        private double altura;
        public double GetAltura() { return altura; }
        public void SetAltura(double value) { altura = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public Triangle() { }

        public Triangle(int codi, string nom, Color color, double baseTri, double altura) : base(codi, nom, color)
        {
            triBase = baseTri;
            this.altura = altura;
        }

        public Triangle(Triangle tri)
        {
            SetCodi(tri.GetCodi());
            SetNom(tri.GetNom());
            SetColor(tri.GetColor());
            SetBase(tri.GetBase());
            SetAltura(tri.GetAltura());
        }

        // Metodo abstracto
        public override double CalcularArea() { return (triBase * altura) / 2; }

        // Metodo
        public override string ToString() { return $"{base.ToString()}, Base={triBase}, Altura={altura}, Area={CalcularArea()}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) { return false; }

            Triangle other = (Triangle)obj;
            return GetCodi() == other.GetCodi();
        }

        public override int GetHashCode() { return GetCodi().GetHashCode(); }

        // Metodo Comparar de la interficie
        public int Comparar(IOrdenable otraFigura)
        {
            if (this == null || otraFigura == null) { return 0; }
            double a0 = CalcularArea();
            double a1 = 0;

            if (otraFigura is Triangle)
            {
                Triangle triangle = (Triangle)otraFigura;
                a1 = triangle.CalcularArea();
            }

            if (otraFigura is Cercle)
            {
                Cercle cer = (Cercle)otraFigura;
                a1 = cer.CalcularArea();
            }

            if (otraFigura is Rectangle)
            {
                Rectangle rect = (Rectangle)otraFigura;
                a1 = rect.CalcularArea();
            }

            if (a0 == a1) { return 0; }
            else if (a0 < a1) { return -1; }
            else { return 1; }
        }
    }
}
