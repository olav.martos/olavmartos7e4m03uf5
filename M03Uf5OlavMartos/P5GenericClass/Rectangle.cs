﻿using System.Drawing;

namespace P5GenericClass
{
    public class Rectangle : FiguraGeometrica, IOrdenable
    {
        // Atributos
        private double rectBase;
        public double GetBase() { return rectBase; }
        public void SetBase(double value) { rectBase = value; }


        private double altura;
        public double GetAltura() { return altura; }
        public void SetAltura(double value)
        { altura = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public Rectangle() { }
        public Rectangle(int codi, string nom, Color color, double baseRect, double altura) : base(codi, nom, color)
        {
            rectBase = baseRect;
            this.altura = altura;
        }
        public Rectangle(Rectangle rect)
        {
            SetCodi(rect.GetCodi());
            SetNom(rect.GetNom());
            SetColor(rect.GetColor());
            SetBase(rect.GetBase());
            SetAltura(rect.GetAltura());
        }

        // Metodo abstracto
        public override double CalcularArea() { return rectBase * altura; }

        // Metodos
        private double Perimetre() { return 2 * (rectBase + altura); }
        public override string ToString() { return $"{base.ToString()}, Base={rectBase}, Altura={altura}, Perimetre={Perimetre()}, Area={CalcularArea()}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) { return false; }

            Rectangle other = (Rectangle)obj;
            return GetCodi() == other.GetCodi();
        }

        public override int GetHashCode() { return GetCodi().GetHashCode(); }

        // Metodo Comparar de la interficie
        public int Comparar(IOrdenable otraFigura)
        {
            if(this == null || otraFigura == null) { return 0; }
            double a0 = CalcularArea();
            double a1 = 0;

            if(otraFigura is Triangle) { 
                Triangle triangle = (Triangle)otraFigura;
                a1 = triangle.CalcularArea();
            }

            if(otraFigura is Cercle)
            {
                Cercle cer = (Cercle)otraFigura;
                a1 = cer.CalcularArea();
            }

            if(otraFigura is Rectangle)
            {
                Rectangle rect = (Rectangle)otraFigura;
                a1 = rect.CalcularArea();
            }

            if(a0==a1) { return 0; }
            else if(a0<a1) { return -1; }
            else { return 1; }
        }
    }
}
