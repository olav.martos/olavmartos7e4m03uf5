﻿using System.Collections.Generic;

namespace P5GenericClass
{
    public class Algorismes
    {
        /// <summary>
        /// Funcion que ordena una tabla de objetos gracias al metodo Comparar que poseen cada clase de objetos que contienen dicho metodo
        /// que pertenece a la Interficie IOrdenable.
        /// </summary>
        /// <param name="taula">Tabla de objetos que ordenar</param>
        /// <typeparam name="T">Tipo de parametro generico</typeparam>
        internal static void Ordenar<T>(T[] taula) where T : IOrdenable
        {
            for (int i = 0; i < taula.Length - 1; i++)
            {
                for (int j = 0; j < taula.Length - i - 1; j++)
                {
                    // El metodo Comparar puede devolver diversos numeros, entre ellos el 1. Si devuelve este numero se ordenara los dos elementos que estaba comparando
                    if(taula[j]==null) { }
                    else if (taula[j].Comparar(taula[j + 1]) > 0)
                    {
                        T temp = taula[j];
                        taula[j] = taula[j + 1];
                        taula[j + 1] = temp;
                    }
                }
            }
        }


        /// <summary>
        /// Lo mismo que el de arriba pero usando una lista
        /// <para>No se usa en la practica pero aqui se queda</para>
        /// </summary>
        /// <typeparam name="T">Tipo de parametro generico</typeparam>
        /// <param name="taula">Tabla de objeto pasada como parametro</param>
        internal static void Ordenar<T>(List<T> taula) where T : IOrdenable
        {
            for (int i = 0; i < taula.Count - 1; i++)
            {
                for (int j = 0; j < taula.Count - i - 1; j++)
                {
                    // El metodo Comparar puede devolver diversos numeros, entre ellos el 1. Si devuelve este numero se ordenara los dos elementos que estaba comparando
                    if (taula[j].Comparar(taula[j + 1]) > 0)
                    {
                        T temp = taula[j];
                        taula[j] = taula[j + 1];
                        taula[j + 1] = temp;
                    }
                }
            }
        }
    }
}
