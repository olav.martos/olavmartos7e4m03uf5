﻿using System;
using System.Drawing;

namespace P5GenericClass
{
    class ProvaTaulaOrdenableFiguraGeometrica
    {
        static void Main()
        {
            // Creamos la TaulaOrdenableFiguraGeometrica
            TaulaOrdenableFiguraGeometrica<IOrdenable> to = new TaulaOrdenableFiguraGeometrica<IOrdenable>(50);

            // Creamos el rectangulo, los dos circulos y los dos triangulos.
            Rectangle rectangle = new Rectangle(1, "rectangulo", Color.White, 4, 8);
            Cercle cercle1 = new Cercle(2, "circulo1", Color.Red, 7);
            Cercle cercle2 = new Cercle(3, "circulo2", Color.YellowGreen, 6);
            Triangle triangle1 = new Triangle(4, "triangulo1", Color.Blue, 7, 5);
            Triangle triangle2 = new Triangle(5, "triangulo2", Color.Purple, 4, 7);

            // Añadimos los objetos y los mostramos
            to.Afegir(rectangle);
            to.Afegir(cercle1);
            to.Afegir(cercle2);
            to.Afegir(triangle1);
            to.Afegir(triangle2);
            to.Visualitzar();

            // Ordenamos y visualizamos
            Console.WriteLine("\n\nOrdenacion de tabla en marcha");
            to.Ordenar();
            to.Visualitzar();

            // Probamos ExemplarAt con el objeto en la posicion 2 despues de Ordenar
            Console.WriteLine("\n\n\n\nPrueba de ExemplarAT y ExtreureAT");
            to.ExemplarAT(2);
            to.ExtreureAT(2);
            to.Visualitzar();

            Console.WriteLine("Pulsa cualquier tecla para vaciar la tabla");
            Console.ReadLine();
            Console.Clear();
            to.Buidar();
            to.Visualitzar();
        }

        static void PruebasGenerales()
        {

            // Creamos una lista de figuras con tres rectangulos como contenido
            IOrdenable[] figuras = new IOrdenable[10];
            figuras[0] = (new Rectangle(3, "r", Color.White, 4, 8));
            figuras[1] = (new Rectangle(1, "r", Color.White, 2, 8));
            figuras[2] = (new Rectangle(2, "r", Color.White, 3, 8));

            // Pasamos la lista de figuras a la clase TaulaOrdenable
            TaulaOrdenable<IOrdenable> to = new TaulaOrdenable<IOrdenable>(figuras);

            // Visualizamos el contenido de la tabla
            to.Visualitzar();


            // Añadimos un nuevo rectangulo
            Rectangle rect4 = new Rectangle(4, "Rect5", Color.Blue, 20, 12);
            to.Afegir(rect4);


            // Añadimos un nuevo rectangulo
            Triangle rect5 = new Triangle(5, "Tri1", Color.Blue, 2, 4);
            int resultado = to.Afegir(rect5);

            // Comprovamos el numero que devuelve el metodo Afegir()
            if (resultado == 0) { Console.WriteLine("\nEl rectángulo se ha agregado correctamente."); }
            else if (resultado == -1) { Console.WriteLine("\nNo se puede agregar un objeto nulo."); }
            else if (resultado == -2) { Console.WriteLine("\nNo hay suficiente espacio para agregar el rectángulo."); }

            // Ordenamos la tabla y la visualizamos
            to.Ordenar();
            to.Visualitzar();

            // Buscamos un ejemplar en la posicion 2 de la tabla
            int position = 7;
            if (to.ExemplarAT(position) == default) { Console.WriteLine("Valor nulo"); }
            else { Console.WriteLine($"\n\nEjemplar en la posicion {position} {to.ExemplarAT(position)}"); }


            IOrdenable element = to.ExtreureAT(position);
            if (element == default) { Console.WriteLine("Valor nulo"); }
            else { Console.WriteLine($"Ejemplar eliminado en la posicion {position}: {element}\n\n\n\n"); }

            // Mostramos la capacidad y el numero de elementos
            Console.WriteLine("Capacidad: " + to.Capacitat());
            Console.WriteLine("Num Elements: " + to.NrElements());

            // Vaciamos la tabla y vemos su contenido
            Console.WriteLine("\n\n\nVaciamos");
            to.Buidar();
            to.Visualitzar();
        }
    }
}
