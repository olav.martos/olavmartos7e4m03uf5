﻿namespace P5GenericClass
{
    /// <summary>
    /// Interficie IOrdenable creada para la P4M03Uf4OlavMartos
    /// </summary>
    public interface IOrdenable
    {
        /// <summary>
        /// Retorna: 0 si this = x
        /// /// <0 si this < x
        /// >0 si this > x
        /// </summary>
        /// <param name="x">Objeto a comparar</param>
        /// <returns>Devuelve 0 si es igual al siguiente, -1 si es menor que el siguiente y 1 si es mayor que el siguiente</returns>
        int Comparar(IOrdenable x);
    }
}
