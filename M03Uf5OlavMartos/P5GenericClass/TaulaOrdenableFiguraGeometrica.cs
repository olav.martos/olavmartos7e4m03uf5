﻿using System;

namespace P5GenericClass
{
    class TaulaOrdenableFiguraGeometrica<T> where T : IOrdenable
    {
        // Atributos
        TaulaOrdenable<IOrdenable> to;

        // Constructor
        public TaulaOrdenableFiguraGeometrica(int mida)
        {
            if(mida<0) { mida = 10; }
            IOrdenable[] figuras = new IOrdenable[mida];
            to = new TaulaOrdenable<IOrdenable>(figuras);
        }

        // Metodos, debido a la existencia de esta clase, lo que se hace es crear metodos que llamen a los metodos de TaulaOrdenable y que
        // tienen los mismos nombres por lo que se podria pensar que son los mismos, pero no lo son
        public int Capacitat() { return to.Capacitat(); }

        public int NrElements() { return to.NrElements(); }

        public T ExemplarAT(int position) 
        {
            IOrdenable element = to.ExemplarAT(position);
            if (element == default) { Console.WriteLine("Valor nulo"); }
            else { Console.WriteLine($"Ejemplar en la posicion {position}: {element}\n\n\n\n"); }
            return (T)element; 
        }

        public T ExtreureAT(int position) 
        {
            IOrdenable element = to.ExtreureAT(position);
            if (element == default) { Console.WriteLine("Valor nulo"); }
            else { Console.WriteLine($"Ejemplar eliminado en la posicion {position}: {element}\n\n\n\n"); }
            return (T)element;
        }

        public void Buidar() { to.Buidar(); }

        public void Visualitzar() { to.Visualitzar(); }

        /// <summary>
        /// Metodo Afegir para Rectangles, no se porque, pero no me dejaba que funcionase con T obj, asi que se han creado tres
        /// metodos unicamente cambiando la clase del parametro
        /// </summary>
        /// <param name="obj">Rectangulo a añadir</param>
        /// <returns>Un numero, que puede ser 0, -1 o -2</returns>
        internal int Afegir(Rectangle obj)
        {
            int resultado = to.Afegir(obj);
            if (resultado == 0) { Console.WriteLine("\nEl objeto se ha agregado correctamente."); }
            else if (resultado == -1) { Console.WriteLine("\nNo se puede agregar un objeto nulo."); }
            else if (resultado == -2) { Console.WriteLine("\nNo hay suficiente espacio para agregar el objeto."); }
            return resultado;
        }

        /// <summary>
        /// Metodo Afegir para Cercles, no se porque, pero no me dejaba que funcionase con T obj, asi que se han creado tres
        /// metodos unicamente cambiando la clase del parametro
        /// </summary>
        /// <param name="obj">Circulo a añadir</param>
        /// <returns>Un numero, que puede ser 0, -1 o -2</returns>
        internal int Afegir(Cercle obj)
        {
            int resultado = to.Afegir(obj);
            if (resultado == 0) { Console.WriteLine("\nEl objeto se ha agregado correctamente."); }
            else if (resultado == -1) { Console.WriteLine("\nNo se puede agregar un objeto nulo."); }
            else if (resultado == -2) { Console.WriteLine("\nNo hay suficiente espacio para agregar el objeto."); }
            return resultado;
        }

        /// <summary>
        /// Metodo Afegir para Triangles, no se porque, pero no me dejaba que funcionase con T obj, asi que se han creado tres
        /// metodos unicamente cambiando la clase del parametro
        /// </summary>
        /// <param name="obj">Triangulo a añadir</param>
        /// <returns>Un numero, que puede ser 0, -1 o -2</returns>
        internal int Afegir(Triangle obj)
        {
            int resultado = to.Afegir(obj);
            if (resultado == 0) { Console.WriteLine("\nEl objeto se ha agregado correctamente."); }
            else if (resultado == -1) { Console.WriteLine("\nNo se puede agregar un objeto nulo."); }
            else if (resultado == -2) { Console.WriteLine("\nNo hay suficiente espacio para agregar el objeto."); }
            return resultado;
        }

        public void Ordenar() { to.Ordenar(); }
    }
}
