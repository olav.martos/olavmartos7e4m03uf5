﻿/*
 * NAME: Olav Martos Aceña
 * DATE: 13/04/2023
 * DESCRIPTION: Ejercicio 1 de las collections de M3-UF5.
 */

using System;

namespace UF5CollectionsAtencionClienteBanco
{
    class AtencionClienteBanco
    {
        static void Main(string[] args)
        {
            ColaCliente cola = new ColaCliente();
            string decision = "no";
            do
            {
                /* Iniciamos el bucle preguntando la nueva solicitud, luego mostramos la siguiente,
                  preguntamos si se ha atendido la primera solicitud y mostramos las pendientes*/
                Console.Clear();
                NuevaSolicitud(cola);
                cola.MostrarSiguienteSolicitud();
                SolicitudAntendida(cola);
                cola.SolicitudesPendientes();

                // Preguntamos al usuario si quiere salir del programa o no
                Console.Write("Desea salir del programa? (Si/No) ___ ");
                decision = Console.ReadLine();
            } while (decision.ToLower() != "si");

            endExercise();
        }

        /// <summary>
        /// Codigo que se ejecuta para saber que el programa ha terminado
        /// </summary>
        static void endExercise()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa una tecla per terminar");
            Console.ForegroundColor = ConsoleColor.White;
            return;
        }

        /// <summary>
        /// Creamos una nueva solicitud, preguntando el nombre del cliente y el tipo de solicitud, y la añadimos a la cola
        /// </summary>
        /// <param name="cola">Cola de solicitudes pendientes</param>
        static void NuevaSolicitud(ColaCliente cola)
        {
            Console.WriteLine("Nueva solicitud");
            Console.Write("Nombre del cliente: ");
            string cliente = Console.ReadLine();
            Console.Write("Tipo de solicitud: ");
            string tipo = Console.ReadLine();

            cola.AgregarSolicitud(cliente, tipo);
        }

        /// <summary>
        /// Preguntamos si se ha atendido la primera solicitud y si ha sido atendida se eliminara de la cola.
        /// </summary>
        /// <param name="cola">Cola de solicitudes pendientes</param>
        static void SolicitudAntendida(ColaCliente cola)
        {
            Console.WriteLine("Se ha atendido su solicitud?");
            string respuesta = Console.ReadLine();
            bool atendida = false;
            switch (respuesta.ToLower())
            {
                case "si":
                case "yes":
                case "verdadero":
                case "true":
                case "positivo":
                    atendida = true;
                    break;
                case "no":
                case "falso":
                case "false":
                case "negativo":
                    atendida = false;
                    break;
                default:
                    break;
            }

            // Si ha sido atendida, la eliminamos
            if (atendida)
            {
                cola.EliminarSolicitudAtendida();
            }

        }
    }
}
