﻿namespace UF5CollectionsAtencionClienteBanco
{
    class Solicitud
    {
        public string NombreCliente { get; set; }
        string TipoSolicitud { get; set; }

        public Solicitud(string nombreCliente, string tipoSolicitud)
        {
            NombreCliente = nombreCliente;
            TipoSolicitud = tipoSolicitud;
        }

        public override string ToString()
        {
            return $"{NombreCliente} quiere {TipoSolicitud}";
        }
    }
}
