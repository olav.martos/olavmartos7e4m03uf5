﻿using System;
using System.Collections.Generic;

namespace UF5CollectionsAtencionClienteBanco
{
    class ColaCliente : Queue<Solicitud>
    {
        Queue<Solicitud> cola { get; set; }
        public ColaCliente()
        {
            cola = new Queue<Solicitud>();
        }

        public void AgregarSolicitud(string nombreCliente, string tipoSolicitud)
        {
            Solicitud solicitud = new Solicitud(nombreCliente, tipoSolicitud);
            cola.Enqueue(solicitud);
        }

        public void MostrarSiguienteSolicitud()
        {
            Console.WriteLine($"La siguiente solicitud es: {cola.Peek()}");
        }

        public void EliminarSolicitudAtendida()
        {
            // Obtenemos la solicitud eliminada para poder mostrar el nombre del cliente
            Solicitud eliminada = cola.Dequeue();
            Console.WriteLine($"La solicitud de {eliminada.NombreCliente} ha sido eliminada");
        }

        public void SolicitudesPendientes()
        {
            if (cola.Count != 0)
            {
                Console.WriteLine("\nCantidad de solicitudes pendientes\n-----------------------------------------------");
                Console.WriteLine(cola.Count);
            }
            else
            {
                Console.WriteLine("No hay solucitudes pendientes");
            }
        }
    }
}
