﻿
namespace EndlessRunner
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Consola = new System.Windows.Forms.Label();
            this.Dino = new System.Windows.Forms.PictureBox();
            this.Suelo = new System.Windows.Forms.PictureBox();
            this.Cactus1 = new System.Windows.Forms.PictureBox();
            this.Cactus2 = new System.Windows.Forms.PictureBox();
            this.Puntuacion = new System.Windows.Forms.Label();
            this.GameManager = new System.Windows.Forms.Timer(this.components);
            this.pajaro = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Dino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Suelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cactus1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cactus2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pajaro)).BeginInit();
            this.SuspendLayout();
            // 
            // Consola
            // 
            this.Consola.AutoSize = true;
            this.Consola.Location = new System.Drawing.Point(38, 30);
            this.Consola.Name = "Consola";
            this.Consola.Size = new System.Drawing.Size(107, 15);
            this.Consola.TabIndex = 0;
            this.Consola.Text = "console.writeline();";
            // 
            // Dino
            // 
            this.Dino.BackColor = System.Drawing.SystemColors.Control;
            this.Dino.Image = global::EndlessRunner.Properties.Resources.running;
            this.Dino.Location = new System.Drawing.Point(51, 398);
            this.Dino.Name = "Dino";
            this.Dino.Size = new System.Drawing.Size(40, 43);
            this.Dino.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Dino.TabIndex = 1;
            this.Dino.TabStop = false;
            // 
            // Suelo
            // 
            this.Suelo.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Suelo.Location = new System.Drawing.Point(-16, 442);
            this.Suelo.Name = "Suelo";
            this.Suelo.Size = new System.Drawing.Size(1127, 55);
            this.Suelo.TabIndex = 2;
            this.Suelo.TabStop = false;
            // 
            // Cactus1
            // 
            this.Cactus1.Image = global::EndlessRunner.Properties.Resources.obstacle_1;
            this.Cactus1.Location = new System.Drawing.Point(344, 396);
            this.Cactus1.Name = "Cactus1";
            this.Cactus1.Size = new System.Drawing.Size(23, 46);
            this.Cactus1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Cactus1.TabIndex = 3;
            this.Cactus1.TabStop = false;
            this.Cactus1.Tag = "obstaculo";
            // 
            // Cactus2
            // 
            this.Cactus2.Image = global::EndlessRunner.Properties.Resources.obstacle_2;
            this.Cactus2.Location = new System.Drawing.Point(634, 409);
            this.Cactus2.Name = "Cactus2";
            this.Cactus2.Size = new System.Drawing.Size(32, 33);
            this.Cactus2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Cactus2.TabIndex = 4;
            this.Cactus2.TabStop = false;
            this.Cactus2.Tag = "obstaculo";
            // 
            // Puntuacion
            // 
            this.Puntuacion.AutoSize = true;
            this.Puntuacion.Location = new System.Drawing.Point(38, 63);
            this.Puntuacion.Name = "Puntuacion";
            this.Puntuacion.Size = new System.Drawing.Size(38, 15);
            this.Puntuacion.TabIndex = 5;
            this.Puntuacion.Text = "label1";
            // 
            // GameManager
            // 
            this.GameManager.Interval = 20;
            this.GameManager.Tick += new System.EventHandler(this.GameManager_Tick);
            // 
            // pajaro
            // 
            this.pajaro.BackColor = System.Drawing.SystemColors.Control;
            this.pajaro.Image = global::EndlessRunner.Properties.Resources.bird2;
            this.pajaro.Location = new System.Drawing.Point(755, 330);
            this.pajaro.Name = "pajaro";
            this.pajaro.Size = new System.Drawing.Size(70, 51);
            this.pajaro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pajaro.TabIndex = 6;
            this.pajaro.TabStop = false;
            this.pajaro.Tag = "obstaculo";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1108, 526);
            this.Controls.Add(this.pajaro);
            this.Controls.Add(this.Puntuacion);
            this.Controls.Add(this.Cactus2);
            this.Controls.Add(this.Cactus1);
            this.Controls.Add(this.Suelo);
            this.Controls.Add(this.Dino);
            this.Controls.Add(this.Consola);
            this.Name = "Form1";
            this.Text = "DinoGame";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TeclaPulsar);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TeclaSoltada);
            ((System.ComponentModel.ISupportInitialize)(this.Dino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Suelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cactus1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cactus2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pajaro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Consola;
        private System.Windows.Forms.PictureBox Dino;
        private System.Windows.Forms.PictureBox Suelo;
        private System.Windows.Forms.PictureBox Cactus1;
        private System.Windows.Forms.PictureBox Cactus2;
        private System.Windows.Forms.Label Puntuacion;
        private System.Windows.Forms.Timer GameManager;
        private System.Windows.Forms.PictureBox pajaro;
    }
}

