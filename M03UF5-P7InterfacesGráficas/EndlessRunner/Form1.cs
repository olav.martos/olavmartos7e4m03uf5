﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EndlessRunner
{
    public partial class Form1 : Form
    {
        // Atributos
        bool Saltando = false;
        bool inmune = false;
        bool vivo = true;
        int tiempoInmuneOriginal = 100;
        int tiempoInmunidad = 100;
        int PosBase = 398;
        int PosEjeY = 398;
        int FuerzaSalto = 250;
        int puntos = 0;
        int puntosOriginal = 0;
        int VelocidadCactus = 10;
        Random rnd = new Random();

        // Constructor
        public Form1()
        {
            InitializeComponent();
            GameManager.Start();
        }

        // Metodos
        private void TeclaPulsar(object sender, KeyEventArgs e)
        {
            // Si esta vivo podra saltar y activar su inmunidad
            if (vivo)
            {
                // Saltar
                if (e.KeyCode == Keys.Space || e.KeyCode == Keys.Up)
                {
                    Consola.Text = "Saltando...";
                    Saltando = true;
                }

                // Activar inmunidad
                if (e.KeyCode == Keys.Down && !inmune) { Inmunidad(); }
            }

            // Reiniciar partida
            if (e.KeyCode == Keys.R) { Restart(); }

            if (e.KeyCode == Keys.Escape) { Environment.Exit(0); }
        }

        /// <summary>
        /// Activa la inmunidad del dinosaurio durante x tiempo
        /// </summary>
        private void Inmunidad()
        {
            if (vivo)
            {
                inmune = true;
                Consola.Text = $"Dinosaurio inmune durante {tiempoInmunidad} segundos";
            }
        }

        private void TeclaSoltada(object sender, KeyEventArgs e)
        {
            // Si esta vivo podra caer
            if (vivo)
            {
                Consola.Text = "Gravedad activada";
                Saltando = false;
            }
        }

        private void GameManager_Tick(object sender, EventArgs e)
        {
            Dino.Top = PosEjeY;

            // Actualizar Puntuacion
            Puntuacion.Text = "Puntuacion: " + puntos + "\nVelocidad Cactus: " + VelocidadCactus;

            // Comprobamos al dinosaurio
            CheckStatusDino();

            // Comprobamos los elementos
            foreach (Control x in Controls)
            {
                // Mover obstaculo
                if (x is PictureBox && x.Tag == "obstaculo")
                {
                    // Actualizar obstaculo
                    x.Left -= VelocidadCactus;

                    // Movemos los obstaculos a la derecha y cambiamos la altura del pajaro
                    if (x.Left + x.Width < -120)
                    {
                        x.Left = ClientSize.Width + rnd.Next(200, 800);

                        if (x.Name == "pajaro") { x.Top = rnd.Next(100, ClientSize.Height - x.Height - 50); }
                        puntos++;
                    }

                    // Si el dino choca contra un obstaculo y no es inmune morira
                    if (Dino.Bounds.IntersectsWith(x.Bounds) && !inmune) { vivo = false; EndGame(); }
                }
            }

            // Si los puntos llegan a cierta cantidad de puntos la velocidad aumentara
            if (puntos == (puntosOriginal + 20))
            {
                VelocidadCactus += 1;
                puntosOriginal = puntos;
            }

            // Si es inmune, no le ocurrira nada
            if (inmune)
            {
                tiempoInmunidad--;
                Consola.Text = $"Dinosaurio inmune durante {tiempoInmunidad} segundos";

                if (tiempoInmunidad <= 0)
                {
                    inmune = false;
                    Consola.Text = "Inmunidad desactivada";
                    tiempoInmunidad = 100;
                }
            }
        }

        /// <summary>
        /// Codigo del fin del juego
        /// </summary>
        private void EndGame()
        {
            GameManager.Stop();
            Dino.Image = Properties.Resources.dead;
            Consola.Text = "Has muerto\nPulsa 'R' para reiniciar la partida";
            VelocidadCactus = 0;
        }

        private void CheckStatusDino()
        {
            // Si esta saltando, su coord Y sera igual -12.
            if (Saltando) { PosEjeY -= 12; }

            // Si el eje es inferior a la fuerza de salto o no esta saltando, el eje Y aumentara un 12
            if (PosEjeY <= FuerzaSalto || Saltando == false)
            {
                Saltando = false;
                PosEjeY += 12;
            }

            // Si el eje Y es superior al base, el eje Y volvera a ser la base
            if (PosEjeY >= PosBase) { PosEjeY = PosBase; }
        }

        // Codigo que reinicia la partida
        private void Restart()
        {
            // Reiniciar las variables del juego
            Saltando = false;
            inmune = false;
            vivo = true;
            tiempoInmunidad = tiempoInmuneOriginal;
            PosBase = 398;
            PosEjeY = 398;
            FuerzaSalto = 250;
            puntos = 0;
            puntosOriginal = puntos;
            VelocidadCactus = 10;
            rnd = new Random();

            // Reiniciar la imagen del dinosaurio
            Dino.Image = Properties.Resources.running;

            // Reiniciar la posición del dinosaurio
            Dino.Top = PosEjeY;

            // Reiniciar la posición de los obstáculos
            foreach (Control x in Controls)
            {
                if (x is PictureBox && x.Tag == "obstaculo")
                {
                    x.Left = ClientSize.Width + rnd.Next(200, 800);
                }
            }

            // Reiniciar el texto de la consola
            Consola.Text = "";

            // Reiniciar el temporizador del juego
            GameManager.Start();
        }
    }
}
