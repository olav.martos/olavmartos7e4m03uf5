﻿using System;
using System.Collections.Generic;

namespace UF5CollectionSuperTorneoYu_Gi_Oh
{
    class ManoJugador
    {
        // Atributos
        List<Carta> mano { get; set; }

        // Constructor
        public ManoJugador(List<Carta> cartas)
        {
            mano = new List<Carta>(cartas);
        }

        // Metodos

        /// <summary>
        /// Metodo que añade una nueva carta a la mano
        /// <para>Suele ser la que robamos del mazo</para>
        /// </summary>
        /// <param name="nuevaCarta">La carta que queremos añadir</param>
        public void AgregarCarta(Carta nuevaCarta) { mano.Add(nuevaCarta); }

        /// <summary>
        /// Metodo que usa una carta de la mano y al final la descarta
        /// </summary>
        /// <param name="carta">Carta que queremos jugar</param>
        public void JugarCarta(Carta carta)
        {
            if (mano.Contains(carta))
            {
                Console.Write("Se ha usado ");
                Color(ConsoleColor.Green, carta.Nombre);
                Console.Write(" y aplica su efecto: ");
                Color(ConsoleColor.Blue, carta.Efecto);
                DescartarCarta(carta);
            }
        }

        /// <summary>
        /// Metodo que descarta una carta mientras la mano no este vacia
        /// </summary>
        /// <param name="carta">Carta a eliminar de la mano</param>
        public void DescartarCarta(Carta carta)
        {
            if (mano.Contains(carta) && !EstaVacia())
            {
                mano.Remove(carta);
                Console.Write("\nSe ha descartado ");
                Color(ConsoleColor.Green, carta.Nombre);
                Console.WriteLine($". Quedan {Count()} carta/s\n");
            }
        }

        /// <summary>
        /// Metodo que devuelve si la mano esta vacia o no
        /// </summary>
        /// <returns>True if the .Count is less or equal than 0. False if the .Count is greater than 0</returns>
        public bool EstaVacia()
        {
            if (Count() <= 0)
            {
                Color(ConsoleColor.Cyan, "Mano vacia, adios");
                return true;
            }
            else { return false; }
        }

        /// <summary>
        /// Metodo que pinta una parte deseada de un determinado color
        /// </summary>
        /// <param name="color">Color del que queremos pintar</param>
        /// <param name="atributo">Atributo que mostraremos por pantalla</param>
        private void Color(ConsoleColor color, string atributo)
        {
            Console.ForegroundColor = color;
            Console.Write(atributo);
            Console.ResetColor();
        }

        /// <summary>
        /// Muestra el Count del mano
        /// <para>Se añadio para no tener que hacer mano.mano.Count</para>
        /// </summary>
        /// <returns>Devuelve el .Count de la mano</returns>
        public int Count() { return mano.Count; }

        /// <summary>
        /// Da la primera carta de nuestra mano
        /// </summary>
        /// <returns>Devuelve la primera carta de la mano del jugador, la que esta en posicion 0</returns>
        public Carta PrimeraCarta() { return mano[0]; }
    }
}
