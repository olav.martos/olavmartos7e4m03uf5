﻿using System;
using System.Collections.Generic;

namespace UF5CollectionSuperTorneoYu_Gi_Oh
{
    class MazoCartas : Stack<Carta>
    {
        // Atributos
        Stack<Carta> mazo { get; set; }

        // Constructor
        public MazoCartas(Stack<Carta> mazo)
        {
            this.mazo = mazo;
        }

        // Metodos

        /// <summary>
        /// Metodo que agrega una nueva carta al mazo
        /// </summary>
        /// <param name="nuevaCarta">La carta que queremos añadir al mazo</param>
        public void AgregarCarta(Carta nuevaCarta)
        {
            mazo.Push(nuevaCarta);
        }


        /// <summary>
        /// Metodo que si el mazo no esta vacia, devuelve la ultima carta que ha salido
        /// </summary>
        /// <returns>Devuelve la carta que ha salido</returns>
        public Carta RobarCarta()
        {
            if (!EstaVacio())
            {
                Console.Write($"Se ha robado ");
                Color(ConsoleColor.Green, mazo.Peek());
                return mazo.Pop();
            }
            return null;
        }

        /// <summary>
        /// Metodo que comprueba si el mazo esta vacio
        /// </summary>
        /// <returns>Devuelve true si el contador del mazo es igual o menor a 0 o False si es totalmente superior a 0</returns>
        public bool EstaVacio()
        {
            if (Count() <= 0)
            {
                Console.WriteLine("No puedes robar una carta si el mazo esta vacio");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Metodo que pinta una parte deseada de un determinado color
        /// </summary>
        /// <param name="color">Color del que queremos pintar</param>
        /// <param name="atributo">Atributo que mostraremos por pantalla</param>
        private void Color(ConsoleColor color, Carta atributo)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(atributo + "\n");
            Console.ResetColor();
        }

        /// <summary>
        /// Muestra el Count del mazo
        /// <para>Se añadio para no tener que hacer mazo.mazo.Count</para>
        /// </summary>
        /// <returns>Devuelve el .Count del mazo</returns>
        public new int Count() { return mazo.Count; }

    }
}
