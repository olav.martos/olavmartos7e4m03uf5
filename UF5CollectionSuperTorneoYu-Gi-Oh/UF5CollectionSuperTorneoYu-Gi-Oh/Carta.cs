﻿namespace UF5CollectionSuperTorneoYu_Gi_Oh
{
    class Carta
    {
        // Atributos
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Nivel { get; set; }
        public string Efecto { get; set; }

        // Constructor
        public Carta(string Nombre, string Tipo, int Nivel, string Efecto)
        {
            this.Nombre = Nombre;
            this.Tipo = Tipo;
            this.Nivel = Nivel;
            this.Efecto = Efecto;
        }

        // Metodos
        public override string ToString()
        {
            return $"{Nombre}";
        }
    }
}
