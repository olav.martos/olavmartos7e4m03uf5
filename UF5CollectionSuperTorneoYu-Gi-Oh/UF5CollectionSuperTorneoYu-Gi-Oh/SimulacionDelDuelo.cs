﻿using System;
using System.Collections.Generic;

namespace UF5CollectionSuperTorneoYu_Gi_Oh
{
    class SimulacionDelDuelo
    {
        static void Main(string[] args)
        {
            // Creamos un stack de cartas vacio, creamos una instancia de la clase MazoCartas con ese Stack y le creamos 55 cartas que añadimos
            Stack<Carta> mazoO = new Stack<Carta>();
            MazoCartas mazo = new MazoCartas(mazoO);
            for (int i = 0; i < 55; i++) { CrearCartas(mazo); }

            // Creamos una lista de cartas en las que ponemos cinco cartas robadas del mazo y se lo pasamos a una instancia de la clase ManoJugador
            List<Carta> manoO = new List<Carta>();
            for (int i = 0; i < 5; i++) { manoO.Add(mazo.RobarCarta()); }
            ManoJugador mano = new ManoJugador(manoO);

            // Simulacion
            Console.Clear();
            Simulation(mano, mazo);
        }

        /// <summary>
        /// Crea una nueva carta y la añade al mazo
        /// </summary>
        /// <param name="mazo">Mazo de cartas</param>
        static void CrearCartas(MazoCartas mazo)
        {
            // Listas con datos para crear cartas
            string[] nombres = { "Chevalier Epée", "Le Sceau d'Orichalque", "Raigeki", "Princesse du Blizzard", "Nephtys Des Ténèbres" };
            string[] tipos = { "Fisico", "Especial", "Magico", "Defensa", "Soporte", "Inutil", "Arbol" };
            int[] niveles = { 40, 50, 10, 70, 63, 100, 1 };
            string[] efectos = { "Barrera", "Premonicion", "Tinieblas", "Destruccion Absoluta", "Sanacion", "Psicoquines", "Electrokinesis" };

            // Creamos una variable random y la usamos para seleccionar objetos aleatorios de las arrays para así crear una instancia de Carta
            Random rnd = new Random();
            Carta carta = new Carta(
                nombres[rnd.Next(0, nombres.Length - 1)],
                tipos[rnd.Next(0, tipos.Length - 1)],
                niveles[rnd.Next(0, niveles.Length - 1)],
                efectos[rnd.Next(0, efectos.Length - 1)]
            );

            // Agregamos la nueva carta al mazo
            mazo.AgregarCarta(carta);
        }

        /// <summary>
        /// Simulacion de una partida
        /// </summary>
        /// <param name="mano">Mano de un jugador</param>
        /// <param name="mazo">Mazo de cartas></param>
        static void Simulation(ManoJugador mano, MazoCartas mazo)
        {
            Console.WriteLine("BIENVENIDOS AL DU-DU-DUELO");

            // Mostramos la cantidad inicial del mazo y de la mano
            Contadores(mazo, mano);
            // Jugamos la primera carta de nuestra mano y robamos una carta
            mano.JugarCarta(mano.PrimeraCarta());
            mano.AgregarCarta(mazo.RobarCarta());

            // Jugamos otras cuatro cartas y robamos otras cuatro del mazo
            for (int i = 0; i < 4; i++)
            {
                mano.JugarCarta(mano.PrimeraCarta());
                mano.AgregarCarta(mazo.RobarCarta());
            }

            // Mostramos la cantidad que tiene el mazo y la mano. Y si la mano esta vacia se acaba el programa
            Contadores(mazo, mano);

            // Robamos cartas hasta que el mazo este vacio y comprobamos los contadores
            while (!mazo.EstaVacio()) mano.AgregarCarta(mazo.RobarCarta());
            Contadores(mazo, mano);

            // Jugamos todas nuestras cartas hasta que la mano este vacia
            while (!mano.EstaVacia()) mano.JugarCarta(mano.mano[0]);
        }

        /// <summary>
        /// Mostramos si la mano o el mazo estan vacios y si no lo estan mostramos la cantidad que tienen
        /// </summary>
        /// <param name="mazo">Mazo de cartas</param>
        /// <param name="mano">Mano del jugador</param>
        static void Contadores(MazoCartas mazo, ManoJugador mano)
        {
            // Hacemos una separacion del resto de textos
            int separador = 100;
            Separador(separador);

            // Mostramos los contadores y si la mano esta vacia, el juego finaliza
            if (!mazo.EstaVacio()) { Console.WriteLine("Mazo Count: " + mazo.Count()); }
            if (!mano.EstaVacia()) { Console.WriteLine("Mano Count: " + mano.Count()); }

            // Separacion de los contadores y los textos siguientes
            Separador(separador);
        }

        /// <summary>
        /// Procedimiento cuya unica funcion es crear un separador entre la simulacion y los contadores
        /// </summary>
        /// <param name="separador">Numero de veces que se quiera escribir el simbolo #</param>
        static void Separador(int separador)
        {
            Console.WriteLine();
            for (int i = 0; i < separador; i++) { Console.Write("#"); }
            Console.WriteLine("\n");
        }
    }
}
